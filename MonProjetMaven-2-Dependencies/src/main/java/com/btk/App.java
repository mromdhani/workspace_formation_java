package com.btk;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		int[] tab = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		// Affichage des nombres pairs - Méthode classique
		for (int i = 0; i < tab.length; i++) {
			if (tab[i]%2 == 0)
				System.out.println(i +" : est PAIR ! ");

		};
		
		IntPredicate filtre = x -> x%2 == 0;
		
		int[] pairs = Arrays.stream(tab).filter(filtre ).toArray();
		for (int i : pairs) {
			System.out.println(i);
		};
		
		long[] carrées= Arrays.stream(tab).mapToLong(x -> (long)(x*x)).toArray();
		
		
		int z =0;
	}
}
