package com.btk;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/***
 * 
 * @author Mohamed Romdhani / INSAT, Tunis
 *
 */

public class App {
	public static void main(String[] args) {
		try {
			// Chargement du Connecteur (Driver)
			Class.forName("com.mysql.jdbc.Driver");

			// Connexion sur la BDD
			
			String url = "jdbc:mysql://localhost:3306/BanqueDB";
			String user = "root";
			String password = "password";
			
			Connection connection = 
					DriverManager.getConnection(url, user, password);
			

			// Formulation de la requête SQL
			
			Statement statement = connection.createStatement();

			// Exécution de la requête
			ResultSet resultSet=
					statement.executeQuery("SELECT * FROM COMPTES");

			// Exploitation des résultats
			
			while(resultSet.next()) {
				String num = resultSet.getString("Numero");
				String proprio = resultSet.getString("Proprietaire");
				BigDecimal solde = resultSet.getBigDecimal("Solde");
				
				System.out.println(" Compte : "+ num + " - "+ proprio+ " - "+solde);
				
			}

			// Fermeture de la connexion
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(" ATTENTION , il y a l'exception : "+ e);
		}
	}
}
